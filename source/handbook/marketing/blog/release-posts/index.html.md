---
layout: markdown_page
title: "GitLab Release Posts"
description: "Guidelines to create and update release posts"
---

<br>

### Release Blog Posts Handbook
{:.no_toc}

----

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Release posts

GitLab releases a new version every 22nd of each month,
and announces it through monthly release posts.

Patch and security issues are addressed more often,
whenever necessary.

- For a list of release posts, please check the
category [release](/blog/categories/release/).
- For a list of security releases, please check
the category [security release](/blog/categories/security-release/).
- For a list of features per release, please check
the [release list](/release-list/).
- For all named changes, please check the changelog
for [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CHANGELOG.md)
and [GitLab EE](https://gitlab.com/gitlab-org/gitlab-ee/blob/master/CHANGELOG.md).
- See also [release managers](/release-managers/).

### Templates

To start a new release post, please choose one of
these templates, and follow their instructions
to insert content. Please make sure to use
the most recent template available.

- [Monthly release](#getting-started)
- [Patch release](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/templates/blog/patch_release_blog_template.html.md)
- [Security release](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/templates/blog/security_release_blog_template.html.md)

For patch and security releases, please make sure
to specify them in the title, add the correct [category](../#categories):

- Patch releases: 
  - `title: "GitLab Patch Release: x.x.x and x.x.x"`
  - `categories: release`
- Security releases:
  - `title: "GitLab Security Release: x.x.x and x.x.x"`
  - `categories: security release`

----

## Monthly releases

Monthly releases have an exclusive layout aiming to appraise the reader
with the presentation of a new release every 22nd.

**Note:** The new design for monthly release posts was
 [introduced](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/4780)
in March 2017 with the release of
[GitLab 9.0](/2017/03/22/gitlab-9-0-released/).
The new layout was [introduced](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/5937) in May 2017 with the release of [GitLab 9.2](/2017/03/22/gitlab-9-2-released/).

### Getting started

To create a new monthly release post, add two files to the [about.GitLab.com repository](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/) (consider the release of GitLab X.Y, released in YYYY/MM/DD):

- A YAML data file, containing all the release post content
  - Into `data/release_posts/`, add a new file called `YYYY_MM_22_gitlab_X_Y_released.yml`
  - Template ([latest version](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/templates/blog/YYYY_MM_DD_gitlab_x_y_released.yml))
- A blog post file, containing the introduction and the blog post frontmatter information
  - Into `source/posts/`, add a new file called `YYYY-MM-22-gitlab-X-Y-released.html.md`
  - Template ([latest version](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/templates/blog/release_blog_template.html.md))

**Important!** Please use the most recent templates for each of these files.

#### Merge Request

Create a merge request with the introductory changes _before the kick off call_
to make the post available to receive contributions from the team. Check "Remove source branch when merge request is accepted".
Consider to check "Squash commits when merge request is accepted" only if there are too many commits that are useless (typos, styling, etc...).

The branch name must be `release-X-Y`.

Please use the release post template for your MR:

![release post MR template](release-post-mr-template.png){:.shadow}

**Important**: all the files related to the release process, including `data/features.yml`, `data/mvps.yml` and `data/promo.yml` must be committed in this MR.
{:.alert .alert-info}

#### Authorship

Each month a Product Manager will lead the release post, being accountable for:

- Creating the merge request
- Writing the blog post introduction
- Choosing the [MVP](#mvp) and updating `data/mvps.yml`
- Adding the [cover image](#cover-image) and the [social sharing image](#social-sharing-image)
- Updating the promo file (`source/data/promo.yml`)
- Making sure all feature descriptions are positive and cheerful
- @mentioning `@all` in the MR thread to remind everyone about the post and the due dates
- Helping to solve all comments in the thread
- Making sure all images (png, jpg, and gifs) are smaller than 300 KB each
- Merging the post on the 22nd
- Posting on Social Media (Twitter/Facebook)

#### Stages of contribution

Monthly release posts are created in two stages:

- **General contributions**: everyone can contribute!
In this stage, team members will add features and their
respective images, videos, etc.
- **Review**: 
  - **Content**: PMs check and review the content,
writers/editors copyedit anything necessary
  - **Structure**: tech writing/frontend/ux check the syntax
  and the structure of the blog post.

To be able to finish our work in time, with no rush,
each stage will have its due date.

### Due dates

To having the release post well written and ready in
time for the release date, please set due dates for:

- [General contributions](#general-contributions)
from the team: 6th working day before the 22nd
- On the 16th (or the following workday): Run the release post through an automated spell and grammar check
- [Review](#review): 2nd working day before the 22nd
- On the 20th, mention `@channel` in #release-post, so the last reviews can be done

Ideally, the review should be completed until the 4th working day before the 22nd,
so that the 3rd and the 2nd working day before the release
could be left for fixes and small improvements.

### General contributions

Added by the team until the 6th working day before
the 22nd. Please fill all the [sections](#sections).

#### Accountability

You are responsible for the content you add to the blog post. Therefore,
make sure that:

- all new features in this release are in the release post.
- all the entries are correct and not missing (especially links to
the documentation or feature webpages when available).
- feature availability: add the correct entry (CE, EES, EEP).
- all images are optimized (compressed with [ImageOptim](https://imageoptim.com),
[TinyPNG](https://tinypng.com/), or other tool of your preference) **and** smaller than 300KB.
- if you are adding a gif, it should be compressed as much as possible
and smaller than 300KB. If it's bigger than that, use a video instead.
- all primary features are accompanied by their images.
- all new features [are added to `data/features.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/features.md).

Write the description of every feature as you do
to regular blog posts. Please write according to
the [markdown guide](/handbook/product/technical-writing/markdown-guide/).

## Monthly release blog post sections
{:#sections}

- [Introduction](#introduction)
- [CTA buttons](#cta)
- [MVP](#mvp)
- [Features](#features)
  - [Top feature](#top-feature)
  - [Primary features](#primary-features)
  - [Secondary features (improvements)](#improvements)
  - [Illustrations](#illustrations) (screenshots, gifs, or videos)
  accompanying their respective features
- Performance improvements (added as a secondary feature)
- Omnibus GitLab (added as a secondary feature)
- [Upgrade barometer](#upgrade-barometer)
- [Deprecations](#deprecations)

### Introduction

Add the introduction to the blog post file (`YYYY-MM-DD-gitlab-X-Y-released.html.md`),
in regular markdown.

Add a short paragraph before the `<!-- more -->` separator, and
conclude the intro below it.


```md
Introductory paragraph (regular markdown)

<!-- more -->

Introduction (regular markdown)
```

### CTA

Call-to-action buttons displayed at the end of the introduction.
A CTA to the [events page](/events/) is added by default. Add webcasts,
or custom buttons to this entry whenever necessary.

```yaml
cta:
  - title: Join us for an upcoming event
    link: '/events/'
  - title: Lorem ipsum dolor sit amet
  - link:
```

### MVP

To display the MVP of the month, use the information
provided in this template, and adjust it to your case.
Don't forget to link to the MR with the MPV's contribution.

```yaml
mvp:
  fullname: Dosuken Shinya # full name
  gitlab: dosuken123 # gitlab.com username
  description: | # supports markdown. Please link to the MR with the MVP's contribution.
    Dosuken extended our [Pipelines API](http://docs.gitlab.com/ce/api/pipelines.html#list-project-pipelines)
    by [adding additional search attributes](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/9367).
    This is a huge improvement to our CI API, for example enabling queries to easily return the latest
    pipeline for a specific branch, as well as a host of other possibilities. Dosuken also made a great
    contribution last release, laying the foundation for
    [scheduled pipelines](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/10133). Thanks Dosuken!
```

To choose the MVP, the PM responsible for the blog post will request suggestions from
colleagues and the core team. Based on that, the PM will make a decision.
They should not wait for consensus. There can only be one MVP.

**Important**: remember to update `data/mvps.yml` with the new MVP.
{:.alert .alert-info}

### Features

The most relevant features of the release are included in the post by team members.
Classify the feature according to its relevance and to where you want to place it in the blog post:

#### Top feature

The most important feature of the release, mentioned right after the MVP section.
Images can be added at will in the description entry. A link to the documentation is required.

#### Primary features

Features with higher impact, displayed
in rows after the top feature, with an image next to its text. An image accompanying the description is required.
A [video](#videos) can also be added to replace the image.

#### Secondary features (other improvements)
{:#improvements}

Relevant improvements in GitLab. Image is not required, but recommended.

### Feature blocks

Use feature blocks to add features to the YAML data file.
The layout will be applied automatically by Middleman's
[templating system](/2016/06/10/ssg-overview-gitlab-pages-part-2/#template_engine).

Feature blocks in the YAML data file contain the following entries, as exemplified below:

```yaml
- name: Multi-Project Pipeline Graphs
  available_in: [eep]
  documentation_link: 'https://docs.gitlab.com/ee/ci/pipelines.html#multi-project-pipelines-graphs'
  image_url: '/images/9_3/multi-project_pipelines.png'
  reporter: bikebilly
  issue_url: 'https://gitlab.com/gitlab-org/gitlab-ee/issues/2121'
  description: |
    Lorem ipsum dolor sit amet, [consectetur adipisicing](#link) elit.
```

#### Feature Name

- `name`: feature name, capitalized

Use a short and strong name for all feature names.

#### Feature Availability

Use the following pattern to apply the correct badge to the feature (CE, EES, EEP).

- `available_in`: availability of that feature in GitLab:
  - For Community Edition, use `[ce, ees, eep]`
  - For Enterprise Edition Starter, use `[ees, eep]`
  - For Enterprise Edition Premium, use `[eep]`

#### Reference link (documentation, webpage, etc)

Provide a reference link to the feature whenever possible. It can be, in this priority order:

- A **feature webpage**, when available
- A **feature documentation** link, when available
- A **feature-related documentation** link, when both above are not available.

**Important**: always link to the EE documentation, even if the feature is available in CE.
{:.alert .alert-info}

As follows:

- `documentation_link`: use to insert a link to the documentation, or, when available, link preferably to the feature webpage (e.g. Issue Boards, Cycle Analytics, Pages, Geo).
  - `documentation_link` is required for the top feature and optional for primary and secondary features.
- `documentation_text`: use to customize the text for `documentation_link`:
  - If you don't use `documentation_text` at all, the fallback is "_Read through the documentation on `feature name`_". When the documentation link corresponds to the feature name, simply opt for omitting `documentation_tex` and using the fallback.
  - Whenever the feature name doesn't match the feature documentation, use the `documentation_text` entry to adjust the text. E.g., if the feature name is "Improved Pipeline Graphs", and it links to the pipelines doc, `documentation_text: Read through the documentation on Pipelines`.
  - Whenever you link to a webpage or anything rather than GitLab's documentation, use "Learn more about X: `documetation_text: Learn more about Feature Name`. E.g., if the feature name is "Improved GitLab Pages UX", `documentation_link: '/features/pages/'`, `documentation_text: Learn more about GitLab Pages`
  - `documentation_text` is available for all feature blocks that include `documentation_link`.

#### Illustration (images, videos)

- `image_url`: link to the image which illustrates that feature.
Required for primary features, optional for secondary features and top feature.
- `image_noshadow: true`: if an image (`image_url`) already has shadow
the entry `image_noshadow` will remove the shadow applied with CSS by default. Optional.
- `video`: when present, overrides the image and displays the linked video instead. Use the [link for embed videos](/handbook/product/technical-writing/markdown-guide/#videos). Available for primary features only. For all other blocks, add it into the description entries.

Check the section **Adding Content** > [Illustrations](#illustrations) for more information.

#### Feature Reporter

- `reporter`: GitLab handle of the user adding the feature block in the list (not the feature author)
This should be the PM responsible for the feature, so in the review phase anyone knows who they have to ping in order to get clarifications.
It is a required field.

#### Related issue

- `issue_url`: link to the issue on GitLab.com where the feature is discussed and developed.
Using this link the reviewer can check the status of the specific feature for consistency and additional references.
It is a required field, if there is no issue related to the specific entry, put 'none'

#### Feature Description

- `description: |`: add the feature's description in this entry.
Make sure your cursor is in the line below the pipeline symbol `|` intended once.
All `description` fields fully support
[markdown](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/),
the only thing you need to be worried about is respecting the indentation.

### Cover image license

According to our [Blog handbook](/handbook/marketing/blog/#cover-image), it's necessary
to provide the source of the cover image. Fill in the entry below to display
this info at the very end of the blog post:

```yaml
cover_img:
  image_url: '#link_to_original_image'
  licence: CC0 # which licence the image is available with
  licence_url: '#link_to_licence'
```

### Upgrade barometer

Describes the information about upgrading GitLab to the new version.

```yaml
barometer:
  description: |
    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
    Dignissimos blanditiis reprehenderit voluptate ea quidem
    eveniet similique tempore [quibusdam fugiat](#link) magni, eius,
    quasi aperiam corrupti `tempora` rerum amet totam maiores.
    Reiciendis.
```

### Extras

If you need an extra block to convey important info, and it doesn't fit the other blog post sections, you can use the `extras` block, right before `deprecations` (in the release post YAML datafile):

```yaml
extras:
  - title: "Hello World"
    description: | # supports markdown
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur, beatae!
```

For more multiple blocks, use:


```yaml
extras:
  - title: "Hello World"
    description: | # supports markdown
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur, beatae!
  - title: "Lorem"
    description: | # supports markdown
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque.
```

### Deprecations

Describe the deprecations happening on that release or in upcoming releases.
Let our community know about a future deprecation as soon as possible.

```yaml
deprecations:
  - feature_name: Lorem ipsum dolor
    due: May 22nd, 2017. # example
    description: |  # example (supports markdown)
      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
      Veritatis, quisquam.
```

For multiple deprecations, use multiple feature deprecation blocks:

```yaml
deprecations:
  - feature_name: Lorem ipsum dolor
    due: May 22nd, 2017. # example
    description: |  # example (supports markdown)
      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
      Veritatis, quisquam.
  - feature_name: Lorem ipsum dolor
    due: May 22nd, 2017. # example
    description: |  # example (supports markdown)
      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
      Veritatis, quisquam.
```

### Review

The review is performed after content has been added, so it's important
to respect the due dates, otherwise reviews will have to be done repeatedly.

Ideally, the review should be completed by the 4th working day before the 22nd,
so that the 3rd and the 2nd working day before the release
can be left for fixes and small improvements.

#### Content review

The content review is performed by product managers (PMs),
who will check if everything is in place, and if there's
nothing missing.

Technical writers/editors will follow with
copyedit and check for grammar, spelling, and typos.
Please follow the checklist in the MR description to
guide you through the review.

Lastly, on the 2nd day before the 22nd (or as soon as the content is ready),
the post should be reviewed by a Marketing team member (PMM, CMM)
to evaluate wording and messaging.

#### Structural Check

Once the post is filled with content, the technical writing,
UX, or frontend team, will check the syntax and the content structure:

- Filenames
- Frontmatter
  - Authorship
  - Cover image
  - Social sharing image
  - Title
  - Description
  - Category
  - Layout
-  `<!-- more -->` separator (blog intro)
- Deadlinks (Review Apps link)
- Social Sharing card (when published): validate with Twitter Card Validator and Facebook Debugger

##### Frontmatter
{:.gitlab-purple}

Look for each
entry in the frontmatter. Wrap text with double quotes and paths with
single quotes to prevent the page to break due to special chars.

```yaml
---
release_number: "X.X"
title: "GitLab X.X Released with Feature A and Feature B"
author: Name Surname
author_gitlab: gitlab.com-username
author_twitter: twitter-username
categories: release
image_title: '/images/X_X/X_X-cover-image.ext'
description: "GitLab 9.0 Released with XXX, YYY, ZZZ, KKK, and much more!"
twitter_image: '/images/tweets/gitlab-X-X-released.jpg'
layout: release
---
```

## Adding content

### Markdown

For entries that support markdown, use regular
[markdown Kramdown](/handbook/product/technical-writing/markdown-guide/),
as we use for all blog posts and webpages on about.GitLab.com.

### Illustrations

#### Images

- {:#alt} Make sure every image has an
[alternative text](/handbook/product/technical-writing/markdown-guide/#image-alt-text)
- {:#images-compressed} Each image should be compressed with [ImageOptim](https://imageoptim.com),
[TinyPNG](https://tinypng.com/), or similar tool
- {:#image-size-limit} Each image should not surpass 300KB, gifs included
- {:#gifs} **Animated gifs**:
  - If a gif isn't necessary, replace it with a static image (.png, .jpg)
  - If an animation is necessary but the gif > 300KB, use a video instead
- {:#cover-image} **Cover image**:
use an unique image as cover to every post, and add
[the required copyright info](#cover-image-license) into the Yaml file.
This image should be eyes-catching and inspiring. Suggested aspect ratio is 3:1 and resolution should be enough to be good-looking on big displays.
- {:#image-shadow} **Image shadow**:
when you add images though the text,
make sure all images have the class shadow applied:
  - `![image alt text](#img-url){:.shadow}`
  - If the original image already has shadow applied, don't use `{:.shadow}`.
  - If you're inserting the image in the YAML file via `image_url` entry, add the `image_noshadow: true` [entry](#feature-blocks) right after `image_url`.
- {:#social-sharing-image} **Social sharing image**:
It's recommended to add a [social sharing image](/handbook/marketing/social-marketing/#defining-social-media-sharing-information)
to the blog post. It is the image that will display on
social media feeds whenever the link to the post is shared.
The image should be placed under `source/images/tweets/`
and named after the post's filename (`gitlab-X-X-released.png`).

#### Videos

Every [video should wrapped into a figure tag](/handbook/product/technical-writing/markdown-guide/#videos), as in:

```html
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/PoBaY_rqeKA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
```

The `<figure>` element is recommended for semantic SEO
and the `video_container` class will assure the video
is displayed responsively.

Consult the [markdown guide](/handbook/product/technical-writing/markdown-guide/#videos)
for the correct markdown markup to apply to different sources (YouTube, Google Drive, HTML video).

For `primary_features`, you can add a video instead of an image, by using the entry `video:`.
If present, the feature section won't display any images, only the video. Example:

```yaml
# PRIMARY FEATURES
  primary:
    - name: Awesome Feature
      available_in: [ce, ees, eep]
      documentation_link: ''
      documentation_text: "Learn more"
      video: "https://www.youtube.com/embed/eH-GuoqlweM"
      description: |
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, provident.
```

## Technical aspects

Understand how a release post is formed:

- **Template:**
  - [Layout (Haml) file](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/layouts/release.haml): 
  creates a layout for the final HTML file, and requires the include file below.
  - [Include (Haml) file](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/includes/release.html.haml): 
  builds the content of the post applying custom styles. Its markup includes semantic SEO improvements.
- **Content:**
  - **YAML data file**: gathers the actual content for the blog post, except the introduction ([template](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/templates/blog/YYYY_MM_DD_gitlab_x_y_released.yml), [example](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/release_posts/2017_05_22_gitlab_9_2_released.yml))
  - **Blog post file**: the blog post file, which holds the introduction of the blog post and its frontmatter ([template](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/templates/blog/release_blog_template.html.md), [example](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/posts/2017-05-22-gitlab-9-2-released.html.md))

The template files form the blog post, therefore, don't need to be changed every release.
The content files are the ones to be added every release with its unique content, as described
by the section [getting started](#getting-started).

To learn more how the templating system works, read through an overview on
[Modern Static Site Generators](/2016/06/10/ssg-overview-gitlab-pages-part-2/).

<style>
  pre { margin-bottom: 20px; }
</style>
